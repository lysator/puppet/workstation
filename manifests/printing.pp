class workstation::printing {
  file { '/usr/share/cups/model/xrx6510.ppd':
    source => 'puppet:///modules/workstation/printing/xrx6510.ppd',
    owner  => 'root',
    group  => 'lp',
    mode   => '0640',
  }

  cups_queue { 'urd':
    ensure         => 'printer',
    accepting      => true,
    access         => {
      'policy' => 'allow',
      'users'  => ['all']
    },
    description    => 'Lysators skrivare i ~',
    enabled        => true,
    held           => false,
    location       => '~',
    make_and_model => 'Xerox Phaser 6510',
    options        => {
      'auth-info-required'   => 'none',
      'job-k-limit'          => '0',
      'job-page-limit'       => '0',
      'job-quota-period'     => '0',
      'job-sheets-default'   => 'none,none',
      'port-monitor'         => 'none',
      'printer-error-policy' => 'stop-printer',
      'printer-op-policy'    => 'default',
      'XRXOptionDuplex'      => 'True',
      'PageSize'             => 'A4',
      'Collate'              => 'True',
      'InputSlot'            => 'Tray1',
      'MediaType'            => 'Unspecified',
      'MediaColor'           => 'White',
      'Duplex'               => 'DuplexNoTumble',
      'OutputMode'           => 'Enhanced',
      'JCLBanner'            => 'False',
      'JCLJobType'           => 'Normal'
    },
    ppd            => '/usr/share/cups/model/xrx6510.ppd',
    provider       => 'cups',
    shared         => false,
    uri            => 'ipp://urd.lysator.liu.se',
  }

  class { '::cups':
    default_queue => 'urd',
  }
}
