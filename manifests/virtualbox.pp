class workstation::virtualbox {

  file { '/etc/yum.repos.d/virtualbox.repo':
    ensure => file,
    source => 'https://download.virtualbox.org/virtualbox/rpm/fedora/virtualbox.repo'
  }

  file { '/opt/oracle_vbox.asc':
    ensure => file,
    source => 'https://www.virtualbox.org/download/oracle_vbox.asc',
  } ~> exec { 'rpm --import /opt/oracle_vbox.asc':
    path   => ['/bin', '/usr/bin',],
    unless => "/usr/bin/rpm -qi 'gpg-pubkey-*' | /usr/bin/grep -q '^Packager[ ]*:.*<info@virtualbox.org>$'",
  }


  $version = $facts['kernelrelease']
  ensure_packages([
    'VirtualBox-6.1',
    "kernel-devel-${version}",
  ], {
    ensure => installed,
    notify => Exec['/sbin/vboxconfig'],
  })
  exec { '/sbin/vboxconfig':
    refreshonly => true,
  }

}
