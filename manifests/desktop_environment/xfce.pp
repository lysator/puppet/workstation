# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include workstation::desktop_environment::xfce
class workstation::desktop_environment::xfce (
  Array[String] $packages = undef,
){
  ensure_packages($packages)
}
