# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include workstation::desktop_environment::gnome
class workstation::desktop_environment::gnome (
  $packages = undef,
){
  ensure_packages($packages)
}
