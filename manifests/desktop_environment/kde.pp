class workstation::desktop_environment::kde (
  $packages = []
) {
  ensure_packages($packages)
}
