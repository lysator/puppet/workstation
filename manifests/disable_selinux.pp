class workstation::disable_selinux {
  # The change does not take effect until the system is restarted, so switch to
  # permissive mode as a stop-gap.
  file_line { 'disable selinux':
    ensure => present,
    path   => '/etc/selinux/config',
    line   => 'SELINUX=disabled',
    match  => '^SELINUX=',
  }
  ~> exec { '/usr/sbin/setenforce 0':
    refreshonly => true,
  }
}
