# Install non-free media libs
# In Fedora 36, they begun shipping ffmpeg, but only with the free libs. The packages
# introduced conflict with the non-free packages from rpmfusion, and it's non-trivial
# to fix in puppet using only package resources
#
# https://rpmfusion.org/CommonBugs#FFmpeg-free%20conflicts
# https://docs.fedoraproject.org/en-US/quick-docs/assembly_installing-plugins-for-playing-movies-and-music/
class workstation::media_libs {
  exec { 'swap ffmpeg-free ffmpeg':
    command =>  '/usr/bin/dnf swap -y ffmpeg-free ffmpeg --allowerasing',
    onlyif  => '/usr/bin/rpm -q ffmpeg-free',
  }

  exec { "/usr/bin/dnf install -y gstreamer1-plugins-base 'gstreamer1-plugins-bad-*' 'gstreamer1-plugins-good-*' gstreamer1-plugins-ugly  gstreamer1-plugin-openh264 gstreamer1-libav 'lame*'":
    subscribe   => Exec['swap ffmpeg-free ffmpeg'],
    refreshonly => true,
  }

  exec { '/usr/bin/dnf group upgrade -y --with-optional Multimedia':
    subscribe   => Exec['swap ffmpeg-free ffmpeg'],
    refreshonly => true,
  }
}
