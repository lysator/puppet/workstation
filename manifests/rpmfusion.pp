class workstation::rpmfusion {
  ::workstation::rpmfusion::repo { 'rpmfusion-free-release':
    rpm_url  => "https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-${facts['os']['release']['full']}.noarch.rpm",
    key_path => "/etc/pki/rpm-gpg/RPM-GPG-KEY-rpmfusion-free-fedora-${facts['os']['release']['full']}",
  }

  ::workstation::rpmfusion::repo { 'rpmfusion-nonfree-release':
    rpm_url  => "https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-${facts['os']['release']['full']}.noarch.rpm",
    key_path => "/etc/pki/rpm-gpg/RPM-GPG-KEY-rpmfusion-nonfree-fedora-${facts['os']['release']['full']}",
  }
}

# This should eventually be moved to its own module (since it is general enough to use for arbitrary repos, and not specific to rpmfusion)
define workstation::rpmfusion::repo (
  Pattern[/^https:\/\/.*/] $rpm_url,
  String $key_path,
) {
  # successful exit status if the key is already imported
  $free_check_key_cmd = @("CMD"/$)
    /usr/bin/rpm --quiet -q "gpg-pubkey-$(/usr/bin/gpg --show-keys --with-colons ${key_path} |
      /usr/bin/awk -F : '\$1 == "pub" {print tolower(substr(\$5, 9))}')"
  |-CMD

  package { $name:
    source => $rpm_url,
  }
  -> exec { "import ${name} gpg key":
    command  => "/usr/bin/rpm --import ${key_path}",
    unless   => $free_check_key_cmd,
    provider => shell,
  }
}
