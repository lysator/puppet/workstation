# Install as much of texlive as possible

class workstation::texlive {
  case $facts['os']['name'] {
    'Fedora': {
      # TODO this is only base, add extra equivalent to texlive-full
      ensure_packages([
        'texlive',
      ])
    }
    default: {
      ensure_packages(['texlive-full'])
    }
  }
}
