# @summary Disble automatic sleep/hibernation
class workstation::disable_sleep {
  $keys = [
    'AllowSuspend',
    'AllowHibernation',
    'AllowSuspendThenHibernate',
    'AllowHybridSleep',
  ]
  $changes = $keys.map |$k| { "set Sleep/${k} no" }
  augeas { 'disable sleep':
    incl    => '/etc/systemd/sleep.conf',
    lens    => 'Desktop.lns',
    changes => $changes,
  }
  ~> exec { '/usr/bin/systemctl daemon-reload -- disable sleep':
    command     => '/usr/bin/systemctl daemon-reload',
    refreshonly => true,
  }
}
