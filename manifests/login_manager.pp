# @summary Setup a login manager
#
# @example
#   include workstation::login_manager
class workstation::login_manager (
  $packages = [],
  String $service = undef,
){
  ensure_packages($packages)

  service { $service:
    ensure => running,
    enable => true,
  }
}
