# Install a generic workstation, all bells and whistles

# Add lysator-specific things to the profile instead, or via foreman.
class workstation (
  $desktop_environments = undef, # hiera
  $packages = undef, # hiera
  $package_graveyard = [], # Packages that should be uninstalled
){
  include ::workstation::login_manager

  $desktop_environments.each | $e | {
    include "::workstation::desktop_environment::${e}"
  }

  include workstation::disable_sleep

  include ::workstation::environment

  include ::workstation::sysctl

  include ::workstation::disable_selinux

  include ::workstation::texlive

  include ::workstation::printing

  include ::workstation::media_libs

  if fact('os.name') == 'Fedora' and fact('os.release.major') == '34' {
    include ::workstation::virtualbox
  }

  include ::workstation::rpmfusion

  file { '/lysator':
    ensure => 'link',
    target => '/mp/lysator',
  }

  file { '/var/mail':
    ensure => 'link',
    target => '/mp/mail',
    force  => true,
  }

  if ($facts['hostname'] == 'hades') {
    file { '/etc/X11/xorg.conf.d/10-monitor.conf':
      ensure => file,
      source => 'puppet:///modules/workstation/xorg-monitor.conf-hades',
    }
  }

  file { '/etc/fonts/conf.d/25-no-bitmap-fedora.conf':
    ensure => absent,
  }

  service { 'abrtd':
    ensure => stopped,
    enable => mask,
  }

  # Note that (at least on Fedora) doesn't remove dependant packages,
  # and instead fails. Setting "provder => 'dnf'" didn't work, since
  # that seems to only be for installation.
  ensure_packages($package_graveyard, {
    ensure   => absent,
  })

  ensure_packages($packages)
}
