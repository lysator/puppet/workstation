class workstation::environment (
  $environment = undef,
){
  file { '/etc/profile.d/lysator-env.sh':
    ensure  => file,
    content => $environment.map |$v| { "export ${v[0]}=${v[1]}\n" }.join("")
  }
}
