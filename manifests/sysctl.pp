class workstation::sysctl {
  # https://fedoraproject.org/wiki/QA/Sysrq
  sysctl { 'kernel.sysrq':
    value => '1',
  }
}
