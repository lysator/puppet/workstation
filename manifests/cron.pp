# denna klass installerar cron, där det behövs
class workstation::cron {
  package {
    [
      'cronie',
    ]:
      ensure => installed,
  }
}
